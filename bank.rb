class Account
    attr_reader:name
    attr_reader:balance
    
    def initialize(name, balance=100)
        @name = name
        @balance = balance
    end
    
    private
    def pin
        @pin = 1234
    end
    def pin_error
        return "Acesso negado: PIN incorreto."
    end
    
    public
    def display_balance(pin_number)
        if pin_number == pin
            puts "Saldo: $#{@balance}."
        else
            puts pin_error
        end
    end
     
    public
    def withdraw(pin_number, amount)
        if pin_number == pin
            if amount > @balance
                puts "Você possui saldo insuficiente"
            else
                @balance -= amount
                puts "Retirado #{amount}. Novo Saldo: $#{@balance}."
            end
        else
            puts pin_error
        end
    end
end

class CheckingAccount < Account
   
    def deposit(pin_number, amount)
        if pin_number == pin
            @balance += amount
            puts "Deposito de: #{amount}. Novo Saldo: $#{@balance}."
        else
            puts pin_error
        end
    end
end

account = Account.new("Regiane", 1000)
account.withdraw(1234, 500)
account.withdraw(1238, 200)
account.withdraw(1234, 600)
checking_account = CheckingAccount.new("Flavio", 1500)
checking_account.deposit(1234, 500)
