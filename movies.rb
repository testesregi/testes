movies = { Titanic: 4, StarWars: 7, Shrek: 5, StarTrek: 3, RockV: 6 }

puts "Olá, o que deseja fazer? Digite uma das opções: add, update, display ou delete um filme"

choice = gets.chomp

case choice

when "add"
  puts "Qual fime você gosta?" 
  title = gets.chomp.to_sym

  if movies[title].nil?
    puts "Qual sua avaliação para o filme? (de 1 a 7)"
    rating = gets.chomp.to_i

    if rating >= 1 && rating <= 7
      movies[title] = rating
      puts "Obrigada por sua avaliação!"
    else 
      puts "Poxa, você digitou uma nota inválida :-/"
    end
  else
    puts "Ops! Esse filme já está na tabela!"
  end
  
when "update"
  puts "Entre com um título de filme"
  title = gets.chomp.to_sym

  if movies[title].nil?
     puts "Ele não esta na lista"
  else
    puts "Atualize avaliação para o filme? (de 1 a 7)"
    rating = gets.chomp.to_i

    if rating >= 1 && rating <= 7
      movies[title] = rating
      puts "Obrigada por sua avaliação!"
    else 
      puts "Poxa, você digitou uma nota inválida :-/"
    end
    
  end

when "display"
  movies.each do |title, rating|
    puts "#{title}: #{rating}"  
  end

when "delete"
  puts "Qual filme você quer deletar?"
  title = gets.chomp.to_sym
  
  if movies[title].nil?
    puts "Atenção! Não temos esse filme"
  else 
    movies.delete(title)
  end

else
  puts "Erro!"
end